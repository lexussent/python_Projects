# Import the libraries needed to allow this application to work. Requests to send http request to domains,
# smtplip to allow my email account to send emails on my behalf and os to use environment variables to prevent
# passwords and usernames from being seen in the code.
import requests
import smtplib
import os
import paramiko
import boto3
import time

client = boto3.client('ec2', region_name='eu-north-1')

# Set variables as the environment variables, for this example the same environment variable is used to allow the sender
# to email themselves
SENDER_EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
RECEIVER_EMAIL_ADDRESS = os.environ.get('EMAIL_ADDRESS')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASSWORD')


# Create function to prevent reusing code. Send email function is used to login to the email account on behalf of the
# user and send an email with the contents that are in the message. Uses the environment variables stored earlier for
# security purposes. There is a static starter string for the message, which gets dynamic input from the argument
# provided by the user.
def send_email(email_msg):
    # Sending email to infrastructure team
    with smtplib.SMTP('smtp.gmail.com', 587) as smtp:
        smtp.starttls()
        smtp.ehlo()
        smtp.login(SENDER_EMAIL_ADDRESS, EMAIL_PASSWORD)
        message = f"Subject: Site down \n{email_msg}"
        smtp.sendmail(SENDER_EMAIL_ADDRESS, RECEIVER_EMAIL_ADDRESS, message)


# Function which is used to start the docker container by ssh'ing to the host
def start_service():
    print('Restarting the container service')
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy)
    ssh.connect('16.171.15.180', username='ec2-user', key_filename='/Users/lexus.low/.ssh/testing.pem')
    stdin, stdout, stderr = ssh.exec_command('docker start 00f23137d2b0')
    print(stdout.readlines())
    ssh.close()
    print(' Container has started')

# Uses a try block to catch out connection errors which are different from the status code not equalling 200.
# Basic if statement to confirm that the error response from the request equals 200. If not, then the function calls
# the string which has been stored in the variable "msg" and sends the email.
try:
    response = requests.get('http://16.171.15.180:443')
    if response.status_code == 200:
        print("Service is up and running")
    else:
        print("Service is down")
        msg = f'Error response code: {response.status_code}'
        send_email(msg)
        #Restart the application
        start_service()

except Exception as ex:
    print(f'Connection error happened: {ex}')
    msg = 'Application is not accessible at all, please check on the server '
    send_email(msg)

    # Starts AWS server if the connection is not establish. Uses the boto3 library to send an API request to AWS,
    # to start the server. Checks and wait until the current state is running before attempting to start the container.
    print(' Attempting to start server')
    start = client.start_instances(
        InstanceIds=[
            'i-0734be2be7bc73fbd',
        ],
        AdditionalInfo='string',
        DryRun=False
    )
    for instance in start['StartingInstances']:
        current_state = instance['CurrentState']['Name']
        print(f'The current state of the instance is {current_state}')
        while True:
            if current_state == 'running':
                time.sleep(5)
                start_service()
                break






